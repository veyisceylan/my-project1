# Use the official Alpine Image
FROM alpine

# Install production dependencies.
RUN apk --update add nginx nginx
RUN apk --update add php-fpm
RUN mkdir -p /var/log/nginx
RUN touch /var/log/nginx/access.log
